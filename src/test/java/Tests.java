import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class Tests {
    private static String token = "vk1.a.urKtrhbEAa92mAZemhdkgEf3wyu9UQ8gePukCLAYQdszMrT2T5-_YDCaFwVKCjfZfce2zQooiWAdspcX251QbwV7cQnhUv1aTOJZIkR1Vb9CWh1kTx6-vuCDkGBR6rxRTlCury0wMn8TWnR-gOmUqRudAjBQX0XiFYG0cpr8DnCsUwiI93aFrfKX3La8SKNr";
    @Test
    public void test1() throws IOException {
        String[] args = {token,"{\"user_id\":78389,\"group_id\":93559769}"};
        Main.main(args);
        Assert.assertEquals("{\"member\":false,\"last_name\":\"Гринёв\",\"first_name\":\"Александр\"}",
                Main.response.toString());
    }
    @Test
    public void test2() throws IOException {
        String[] args = {token,"{\"user_id\":537195738,\"group_id\":206383659}"};
        Main.main(args);
        Assert.assertEquals("{\"member\":true,\"last_name\":\"Малоохтинская\",\"first_name\":\"Библиотека\"}",
                Main.response.toString());
    }

    @Test
    public void test3() throws IOException {
        String[] args = {token,"{\"user_id\":7841773,\"group_id\":206383659}"};
        Main.main(args);
        Assert.assertEquals("{\"member\":false,\"last_name\":\"\",\"first_name\":\"DELETED\"}",
                Main.response.toString());
    }
}