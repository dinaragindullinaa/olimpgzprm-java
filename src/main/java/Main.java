import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import org.json.JSONObject;

import java.io.IOException;
import java.net.http.HttpHeaders;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {

    private static String token = null;
    private static Integer userId = null;
    private static Integer groupId = null;
    public static JSONObject response = null;
    public static Logger logger = Logger.getLogger("MyLog.log");

    public static void main(String[] args) {

        FileHandler fh;

        try {
            fh = new FileHandler("src/main/java/MyLog.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            logger.info("Program have started");

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        token = args[0];
        JSONObject object =JsonParser.toJsonObject(args[1]);
        userId = object.getInt("user_id");
        groupId = object.getInt("group_id");
        MyRequest myRequest = new MyRequest();
        JSONObject user = myRequest.getUserRequest(token,userId);
        response = myRequest.makeResponse(token,userId,groupId);
        if(myRequest.correct){
            response = JsonParser.toJsonResponse(user, response);
        }
        System.out.println("My answer is " + response.toString());
    }
}
