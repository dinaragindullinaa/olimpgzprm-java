import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.function.Supplier;

public class MyRequest {
    HttpURLConnection connection = null;
    boolean correct = true;

    private JSONObject sentRequest(String query, Boolean userFlag){
        JSONObject object = null;
        try {
            connection = (HttpURLConnection) new URL(query).openConnection();
            connection.setRequestMethod("GET");
            connection.setUseCaches(false);
            connection.setConnectTimeout(2500);
            connection.setReadTimeout(2500);

            connection.connect();
            StringBuilder sb = new StringBuilder();
            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                while ((line = in.readLine()) != null) {
                    sb.append(line);
                    sb.append("\n");
                }

                object =Check.checkResponse(this,sb.toString(),userFlag);
            } else {
                Main.logger.info("Problem with connection. Try again!");
                System.out.println("Bad!!!! " + connection.getResponseCode());
            }
        } catch (Throwable cause) {
            Main.logger.info((Supplier<String>) cause);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return object;
    }

    public JSONObject getUserRequest(String token, Integer userId){
        String query = "https://api.vk.com/method/users.get?user_ids="+userId+"&extended=1&access_token="+token+"&v=5.131";
        return sentRequest(query,true);
    }

    public JSONObject makeResponse(String token, Integer userId, Integer groupId){
        String query = "https://api.vk.com/method/groups.isMember?group_id="+groupId+"&user_id="+userId+"&extended=1&access_token="+token+"&v=5.131";
        return sentRequest(query,false);
    }

//    public void auth(){
//       String s = "https://oauth.vk.com/authorize?client_id=51468530&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends&response_type=token&v=5.131&state=123456";
//       String ss = "https://oauth.vk.com/authorize?client_id=51468530&redirect_uri=https://oauth.vk.com/blank.html&scope=12&display=mobile&response_type=token";
//       sentRequest(ss,false);
//    }


}
