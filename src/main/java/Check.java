import org.json.JSONObject;

public class Check {
    public static String checkUserId(JSONObject object){
        if (object.getString("first_name").equals("DELETED")){
            return "Account is deleted!";
        }
        else if (object.getString("first_name").equals("BANNED")){
            return "Account is banned!";
        }
        else{
            return  "OK";
        }
    }
    public static JSONObject checkResponse(MyRequest request, String s, boolean flag){
        JSONObject object = JsonParser.toJsonObject(s);
        try{
            object.get("response");
        }
        catch (Exception e){

            s = s.replaceAll("\\{\"error\":","");
            object = JsonParser.toJsonObject(s);

            Main.logger.info("Error message:" + object.get("error_msg"));
            System.out.println("Error message:" + object.get("error_msg"));
            request.correct = false;
            return object;
        }

        try{
            object.get("error");
        }
        catch (Exception e){
            if (flag) {
                s = s.replaceAll("\\{\"response\":\\[", "");
            }
            else{
                s = s.replaceAll("\\{\"response\":", "");
            }
            object = JsonParser.toJsonObject(s);
            if (flag) {
                Main.logger.info("Checked User ID -- "+checkUserId(object));
                System.out.println("Checked User ID -- "+checkUserId(object));
            }
            Main.logger.info(object.toString());
            System.out.println(object);
            return object;
        }
        Main.logger.info("Sorry, i don't know why but it's failed");
        return object;
    }
}
