import org.json.JSONObject;

public class JsonParser {
    public static JSONObject toJsonObject(String s){
        JSONObject object = new JSONObject(s);
        return object;
    }

    public static JSONObject toJsonResponse(JSONObject user, JSONObject responseAnswer){
        JSONObject object = new JSONObject();
        object.put("last_name",user.getString("last_name"));
        object.put("first_name",user.getString("first_name"));
        if (responseAnswer.getInt("member")==0){
            object.put("member", false);
            return object;
        }
        else {
            object.put("member", true);
            return object;
        }
    }
}
